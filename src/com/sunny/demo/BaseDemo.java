package com.sunny.demo;

import com.sunny.common.AsyncCall;

/**
 * create by zsunny
 * data: 2018/8/5
 **/
//demo基类
public abstract class BaseDemo {

    protected AsyncCall asyncCall = new AsyncCall();

    public abstract void callback(long response);

    public void call(){
        System.out.println("发起调用");
        asyncCall.call(this);
        System.out.println("调用返回");
    }

}
